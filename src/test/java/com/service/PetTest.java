package com.service;

import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.model.Pets;
import com.model.User;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestBeanConfig.class })
public class PetTest {

	@Autowired
	private UserServInf userSerInf;
	private Pets pet;
	private User user;
	private List<Pets> setPet;
	private Pets pet1;

	@Before
	public void setup() {
		System.out.println(" Before method..");
		user = new User();
		pet = new Pets();
		pet1 = new Pets();
		setPet = new LinkedList<Pets>();
	}

	@Test
	public void test_saveRegister() {
		// pet.setPet_id(12);
		pet.setPet_name("angel");
		pet.setPet_place("chennai");
		pet.setPet_age(3);
		pet1.setPet_name("pinky");
		pet1.setPet_place("chennai");
		pet1.setPet_age(2);
		setPet.add(pet1);
		setPet.add(pet);
		// user.setUser_id(13);
		user.setUser_name("revathi");
		user.setUser_passwd("abc");
		user.setPets(setPet);
		userSerInf.saveUser(user);
		userSerInf.savepet(user.getUser_id(), pet);
	/*	List<Pets> petList = userSerInf.petList(user.getUser_id());
		for (Pets petObj : petList) {
			Assert.assertNotSame(pet, petObj);
		}*/

	}

	@After
	public void teardown() {
		System.out.println(" After method..");
		user = null;
	}

}
