package com.config;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.model.Pets;
import com.model.User;



@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "com" })
@EnableTransactionManagement
public class PetConfig implements WebMvcConfigurer {
	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	
	@Bean
	public DataSource getDataSource() {
	    BasicDataSource dataSource = new BasicDataSource();
	    dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	    dataSource.setUrl("jdbc:mysql://localhost:3308/test");
	   	    dataSource.setUsername("root");
	    dataSource.setPassword("root");
	    return dataSource;
	}
	
	/*@Bean
	public DataSource getDataSource() {
		
		BasicDataSource dataSource = new BasicDataSource();
	    dataSource.setDriverClassName("org.h2.Driver");
	    dataSource.setUrl("jdbc:h2:file:~/testdb");
	    dataSource.setUsername("sa");
	    dataSource.setPassword("");
	    return dataSource;
	}
	@Bean(initMethod="start",destroyMethod="stop")
	 public org.h2.tools.Server h2WebConsonleServer () throws SQLException {
	   return org.h2.tools.Server.createWebServer("-web","-webAllowOthers","-webDaemon","-webPort", "8082");
	 }
	@Bean
	public DataSource dataSource() {
	  return new EmbeddedDatabaseBuilder()
	    .generateUniqueName(false)
	    .setName("testdb")
	    .setType(EmbeddedDatabaseType.H2)
	    .addDefaultScripts()
	    .setScriptEncoding("UTF-8")
	    .ignoreFailedDrops(true)
	    .build();
	}
*/
	@Bean
	public LocalSessionFactoryBean getsessionFactory() {
		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
		factoryBean.setDataSource(getDataSource());
		 Properties prop=new Properties();
		    String dialect="org.hibernate.dialect.MySQLDialect";
		    prop.setProperty("hibernate.hbm2ddl.auto", "update");
		    prop.setProperty("hibernate.dialect", dialect);
		    factoryBean.setHibernateProperties(prop);

		/*factoryBean.setConfigLocation(context
				.getResource("classpath:hibernate-cfg.xml"));*/
		factoryBean.setAnnotatedClasses(User.class,
				Pets.class);
		return factoryBean;
	}
	
	@Bean
	public HibernateTransactionManager getTransactionManager() {
		HibernateTransactionManager transaction = new HibernateTransactionManager();
		transaction.setSessionFactory(getsessionFactory().getObject());
		return transaction;
	}
	

}
